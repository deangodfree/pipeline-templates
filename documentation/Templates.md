# Introduction

Inside the DevSecOps Governance Framework (DGF) there are three types of templates and each has its own purpose to support the DGF. Secondly, IDE Templates, which are used to define a skeleton `.gitlab-ci.yml` file for consumers of the DGF. Project Templates, which are used to kickstart the creation of new GitLab Projects.

## Pipeline Templates

Pipeline Templates are the backbone of the DGF. All of the jobs are contained within the various `.gitlab-ci.yml` files. You can read the documentation for each of the templates in the links below:

* [Application](PipelineTemplates/Application.md)
* [Build](PipelineTemplates/Build.md)
* [Deployment](PipelineTemplates/Deployment.md)
* [Governance](PipelineTemplates/Governance.md)
* [Helm](PipelineTemplates/Helm.md)
* [Security](PipelineTemplates/Security.md)
* [Terraform](PipelineTemplates/Terraform.md)
* [Test](PipelineTemplates/Test.md)

## IDE Templates

The IDE Templates are stored in a specific repository/project with a specific folder structure and file naming conventions. The template repository can be configured at the instance and/or group levels. Once you set this project as the template repositry in the Group/Admin section of your GitLab Instance, any files following the proper naming convention will appear in the GitLab Web IDE as a priority template at the top of the list. For more information, see the GitLab documentation for [Instance template repository](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html).

Many organizations are used to a Single Standard Pipeline that is used across all repository/projects. Because GitLab Pipelines are based on Declarative Syntax, this makes building a One-Pipeline for all approach very difficult. The solution to this is to build pre-built pipelines in the form of a template, then include them in this repository to be easily consumed.

## Project Templates

The Project Templates are stored in a specific group named "Templates." This group is not special in any way; It becomes special when you set the [instance/group project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) setting. When this is configured, GitLab will look into this group and present any projects in this group as a project template.
