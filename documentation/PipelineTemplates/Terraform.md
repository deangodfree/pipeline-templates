# Terraform

This is a template based on GitLab's Open-Source Terraform implementation. There is some project integration required. The jobs in this template are based on the [GitLab Terraform Template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml). For more information, see [Infrastructure as Code with Terraform and GitLab](https://docs.gitlab.com/ee/user/infrastructure/iac/).

## .terraform-job

This job is used to define the terraform parameters. It is not meant to be invoked directly.

**Image**: [registry.gitlab.com/gitlab-org/terraform-images/stable:latest](https://gitlab.com/gitlab-org/terraform-images/container_registry/)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|TF_ROOT|'${CI_PROJECT_DIR}'|The relative path to the root directory of the Terraform project.|
|TF_STATE_NAME|'${TF_STATE_NAME:-default}'|The name of the state file used by the GitLab Managed Terraform state backend.|

---

## .terraform-fmt

This job builds upon the base `.terraform-job` to enact the `format` function. For more informaton, see [Terraform | format Function](https://www.terraform.io/language/functions/format).

**Usage**

```yaml
terraform-fmt:
  extends: .terraform-fmt
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-init

This job builds upon the base `.terraform-job` to enact the `init` command. For more informaton, see [Terraform | Command: init](https://www.terraform.io/cli/commands/init).

**Usage**

```yaml
terraform-init:
  extends: .terraform-init
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-vaildate

This job builds upon the base `.terraform-job` to enact the `validate` command. For more informaton, see [Terraform | Command: validate](https://www.terraform.io/cli/commands/validate).

**Usage**

```yaml
terraform-vaildate:
  extends: .terraform-vaildate
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-build

This job builds upon the base `.terraform-job` to enact the `plan` command. For more informaton, see [Terraform | Command: plan](https://www.terraform.io/cli/commands/plan).

**Usage**

```yaml
terraform-build:
  extends: .terraform-build
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
    TF_STATE_NAME: "${TF_STATE_NAME:-default}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-deploy

This job builds upon the base `.terraform-job` to enact the `apply` command. For more informaton, see [Terraform | Command: apply](https://www.terraform.io/cli/commands/apply).

**Usage**

```yaml
terraform-deploy:
  extends: .terraform-deploy
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
    TF_STATE_NAME: "${TF_STATE_NAME:-default}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-destroy

This job builds upon the base `.terraform-job` to enact the `destroy` command. For more informaton, see [Terraform | Command: destroy](https://www.terraform.io/cli/commands/destroy).

**Usage**

```yaml
terraform-destroy:
  extends: .terraform-destroy
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}"
    TF_STATE_NAME: "${TF_STATE_NAME:-default}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**

---

## .terraform-unlock

This job builds upon the base `.terraform-job`. It is used to unlock the state file if needed. For more informaton, see [GitLab-managed Terraform state | Manage Terraform state files](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#manage-terraform-state-files).

**Usage**

```yaml
terraform-unlock:
  extends: .terraform-unlock
  variables:
    TF_STATE_NAME: "${TF_STATE_NAME:-default}"
```

**Image**: [.terraform-job image](#terraform-job)

**No Variables**
