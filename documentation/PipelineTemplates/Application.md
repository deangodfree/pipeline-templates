# Application

This file should contain no jobs. It's purpose is to be a 'meta include', i.e. all User-Changeable Global Variables should be defined in this file. In addition, any and all files that you want to include in every pipeline run should be included here at the top. 

Teams will use this file by including it into their `.gitlab-ci.yml` pipeline file. From there they will have access to all the variables and templates included in this file. This file should *NOT* be used as part of a governance or compliance pipeline.

Usage:

```yaml
include:
  - project: pipelinecoe/pipeline-templates
    ref: main
    file: 'Application.gitlab-ci.yml'
```
