# Helm

This is a template based on Helm documentation to support the life cycle of Helm charts. For more information, see [Helm Commands](https://helm.sh/docs/helm/helm/).

Theses templates are also leveraging GitLabs Package Registry; see [Helm charts in the Package Registry ](https://docs.gitlab.com/ee/user/packages/helm_repository/). Other registries can be used by over-riding variables.

Helm jobs are part of the `Helm.gitlab-ci.yml` template, which will be added/extended in your pipeline through the `Application.gitlab-ci.yml`.

## .helm

This job is used to define the Helm variables and the image to be used. It cannot be invoked directly. For more information see [The Helm Package Manager](https://helm.sh/docs/helm/helm/).

**Image**: *custom-registry*/kubernetes-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|HELM_CHART_DIR|"./chart"|The relative path to the directory for Chart.yaml.|
|HELM_CHART_VALUES_PATH|"values.yaml"|The relative path to the file containing.|
|K8S_NAMESPACE|""|The Kubernetes Namespace.|
|HELM_RELEASE_NAME|""|The Release Name.|
|KUBE_CONTEXT|$KUBE_CONTEXT|The Kubernetes context.|
|HELM_CLI_OPTS|""|Any options or flags to be applied to the command.|

---

## .kube-context

This is a reusable script that is leveraged the set the Kubernetes context. For more information see [Kubectl Reference Documentation](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-use-context-em-).

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-deploy

This job builds upon the base `.helm` job to enact the `upgrade` command. It can be used to upgrade a release to a new version of a chart. The `-- install` flag is included, so if a release doesn't already exist, it runs an install. For more information see [Helm Upgrade](https://helm.sh/docs/helm/helm_upgrade/).

**Usage**

```yaml
helm-deploy:
  extends: .helm-deploy
  variables:
    HELM_CHART: "$HELM_CHART_DIR"
    HELM_RELEASE_NAME: "$CI_PROJECT_NAME"
    HELM_CLI_OPTS: "--atomic"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-lint

This job builds upon the base `.helm` job to enact the `lint` command. It examines the chart for possibel issues. For more information see [Helm Lint](https://helm.sh/docs/helm/helm_lint/).

**Usage**

```yaml
helm-lint:
  extends: .helm-lint
  variables:
    HELM_CHART: "$HELM_CHART_DIR"
    HELM_CLI_OPTS: "--quiet"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-package

This job builds upon the base `.helm` job to enact the `package` command. It packages a chart directory into a chart archive. For more information see [Helm Package](https://helm.sh/docs/helm/helm_package/).

**Usage**

```yaml
helm-package:
  extends: .helm-package
  variables:
    HELM_CHART: "$HELM_CHART_DIR"
    HELM_CLI_OPTS: "--version $VERSION"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-publish

This job leverages the GitLab Helm API to upload a package to the projects package registry. For more information see [GitLab Helm API](https://docs.gitlab.com/ee/api/packages/helm.html) and [Helm charts in the Package Registry](https://docs.gitlab.com/ee/user/packages/helm_repository/index.html).

**Usage**

```yaml
helm-publish:
  extends: .helm-publish
  variables:
    HELM_PACKAGE: "$PACKAGE_NAME"
    HELM_PACKAGE_VERSION: "$VERSION"
    HELM_CHANNEL: "stable"
```

**Image**: *Default Image*

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|HELM_PACKAGE|"$PACKAGE_NAME"|Name of the package.|
|HELM_PACKAGE_VERSION|"$VERSION"|Version of the pacakge.|
|HELM_CHANNEL|"stable"|Th channel for the package.|

---

## .helm-push

This job builds upon the base `.helm` job to enact the `push` command. It pushes a chart to a remote registry. This job will require a `before_script` to login into the registry where the package is being pushed. For more information see [Helm Push](https://helm.sh/docs/helm/helm_push/).

**Usage**

```yaml
helm-push:
  extends: .helm-push
  variables:
    HELM_PACKAGE: "$PACKAGE_NAME"
    HELM_PACKAGE_VERSION: "$VERSION"
    REMOTE_REGISTRY: "oci://${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/"
  before_script:
    - aws ecr get-login-password --region "${REGION}" | helm registry login --username "${USERNAME}" --password-stdin "${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com"
```

**Image**: [.helm image](#helm)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|HELM_PACKAGE|"$PACKAGE_NAME"|Name of the package.|
|HELM_PACKAGE_VERSION|"$VERSION"|Version of the pacakge.|
|REMOTE_REGISTRY|"oci://${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/"|Remote registry location.|

---

## .helm-repo

This job builds upon the base `.helm` job to enact the `repo` command and be the base for follow on repo commands. It cannot invoked directly. For more information see [Helm Repo](https://helm.sh/docs/helm/helm_repo/).

**Image**: [.helm image](#helm)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|HELM_REPO_NAME|"${CI_PROJECT_NAME}"|The name of the package repository.|
|HELM_REG_URL|"\${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/"|The URL for the registry.|
|HELM_CHANNEL|"stable"|The channel for the package.|

---

## .helm-repo:add

This job builds upon the base `.helm-repo` job to enact the `add` command. It adds a chart repository. For more information see [Helm Repo Add](https://helm.sh/docs/helm/helm_repo_add/).

**Usage**

```yaml
helm-repo-add:
  extends: .helm-repo:add
  variables:
    HELM_REPO_NAME: "${CI_PROJECT_NAME}"  
    HELM_REG_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/"
    HELM_CHANNEL: "stable"
    HELM_CLI_OPTS: "--force-update"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-repo:index

This job builds upon the base `.helm-repo` job to enact the `index` command. It reads the current directory and generates an index file based on the charts found. For more information see [Helm Repo Index](https://helm.sh/docs/helm/helm_repo_index/).

**Usage**

```yaml
helm-repo-index:
  extends: .helm-repo:index
  variables:
    HELM_CHART_DIR: "./Chart.yaml"  
    HELM_CLI_OPTS: ""
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-repo:list

This job builds upon the base `.helm-repo` job to enact the `list` command. It lists chart repositories. For more information see [Helm Repo List](https://helm.sh/docs/helm/helm_repo_list/).

**Usage**

```yaml
helm-repo-list:
  extends: .helm-repo:list
  variables:
    HELM_CLI_OPTS: "--output json"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-repo:remove

This job builds upon the base `.helm-repo` job to enact the `remove` command. It removes one or more chart repositories. For more information see [Helm Repo Remove](https://helm.sh/docs/helm/helm_repo_remove/).

**Usage**

```yaml
helm-repo-remove:
  extends: .helm-repo:remove
  variables:
    HELM_REPO_NAME: "${CI_PROJECT_NAME}"  
    HELM_CLI_OPTS: "--debug"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-repo:update

This job builds upon the base `.helm-repo` job to enact the `update` command. It gets the latest information about charts from the respective chart repositories. For more information see [Helm Repo Update](https://helm.sh/docs/helm/helm_repo_update/).

**Usage**

```yaml
helm-repo-update:
  extends: .helm-repo:update
  variables:
    HELM_REPO_NAME: "${CI_PROJECT_NAME}"  
    HELM_CLI_OPTS: "--fail-on-repo-update-fail"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-rollback

This job builds upon the base `.helm` job to enact the `rollback` command. It rolls back a release to a previous revision. For more information see [Helm Rollback](https://helm.sh/docs/helm/helm_rollback/).

**Usage**

```yaml
helm-rollback:
  extends: .helm
  variables:
    HELM_REPO_NAME: "${CI_PROJECT_NAME}"  
    REVISION: "$VERSION"
    HELM_CLI_OPTS: "--dry-run"
```

**Image**: [.helm image](#helm)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|REVISION|""|A revision (version) number.|

---

## .helm-test

This job builds upon the base `.helm` job to enact the `test` command. It runs the tests for a release. For more information see [Helm Test](https://helm.sh/docs/helm/helm_test/).

**Usage**

```yaml
helm-test:
  extends: .helm-test
  variables:
    HELM_RELEASE_NAME: "$CI_PROJECT_NAME"
    HELM_CLI_OPTS: "--logs"
```

**Image**: [.helm image](#helm)

**No Variables**

---

## .helm-uninstall

This job builds upon the base `.helm` job to enact the `uninstall` command. It takes a release name and uninstalls the release. For more information see [Helm Uninstall](https://helm.sh/docs/helm/helm_uninstall/).

**Usage**

```yaml
helm-uninstall:
  extends: .helm-uninstall
  variables:
    HELM_RELEASE_NAME: "$CI_PROJECT_NAME"
    HELM_CLI_OPTS: "--dry-run"
```

**Image**: [.helm image](#helm)

**No Variables**
