# Introduction
Welcome to the **DevSecOps Governance Framework (DGF)**. The purpose of this repository is to be a collection of components that can get a customer from 0 to Hero on GitLab CI/CD, very quickly. It is unrealistic to expect everyone in an organization to have the bandwidth to become GitLab experts. The DGF is designed to quickly support and enable one team to be the GitLab CI/CD experts while making best practices, standardization, and code reuse available to everyone in their environment. As team members become more familiar with the functionality and features in GitLab, they can contribute their requirements, ideas, and enhancements.

![Designs/Introduction.png](Designs/Introduction.png)

## Purpose
At its core, the DGF is a library of jobs that can be included in the `.gitlab-ci.yml` of a project's repository. Each of these jobs has a distinct task, i.e. run the maven package command. Each job leverages a hardened container image that has all the dependencies needed to support the task. Stitching together these jobs allows a development team to go from source code to deploying.

Let's look at the components of the DGF.

## Pipeline Templates Project
This project is where all of the CI/CD Job Templates exist, as well as all the DGF source documentation. Inside this project there are numerous `.gitlab-ci.yml` files which contain jobs that make up the DGF Library. The main template that needs to be included in a project's `.gitlab-ci.yml` file is the [`Application.gitlab-ci.yml`](/PipelineTemplates/Application). The jobs span the CI/CD and DevOps processes; planning, building, packaging, testing, scanning, deploying, releasing, and cleanup.

![CI/CD workflow](img/gitlab_workflow_example_extended_v12_3.png)

## Containers Group
The Containers group houses all of the projects to build the containers used in the DGF. We build and use purpose-built containers whose only goal is to run jobs in a CI/CD Pipeline Jobs. They're designed to be small, load quickly, work fast, and destruct quickly. The majority of our containers are built from a [RHEL UBI 8](https://catalog.redhat.com/software/containers/search?q=UBI8%2Fubi&p=1&release_categories=Generally%20Available&architecture=amd64&build_categories_list=Scratch%20image&product_listings_names=Red%20Hat%20Universal%20Base%20Image%208) base container and the base container contains all certificates and configuration necessary to run.

## Templates Group
This group houses all of our commonly used project templates. Each project in here can be deployed as a new project; with a complete CI/CD Pipeline already setup and running. If you're starting a new project, this is a great place to start. Each template is curated and maintained by us. It comes with a complete CI/CD Pipeline and is built for success.

## IDE Templates Project
This group contains all of the file templates for pipelines to be used in the GitLab WebIDE. Any files here will appear in the template dropdown in the WebIDE. The IDE templates can include template files for Dockerfile, .gitignore, .gitlab-ci.yml and Licenses. For more information, see [Supported file ypes and locations](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html#supported-file-types-and-locations).

# Supported Tech Stacks

| Tech Stack | Supported | Description |
| --- | --- | --- |
| NodeJS | Yes | NodeJS is supported 100% |
| Maven | Yes | Maven is supported 100% |
| Python | Yes | Python is supported 100% |
| Gradle | Yes | Gradle is supported 100% |
